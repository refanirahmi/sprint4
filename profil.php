<?php 
	session_start();
	if(!isset ($_SESSION['nama'])){
		echo "anda belum masuk silahkan <a href='masuk.php'>masuk dulu!</a>";
	}else{

	?>
<html>
<head>
	<title>Pondok IT</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<header>
		<ul>
			<a href="index.php"><li>Beranda</li></a>
			<a href="profil.ph"><li>profil</li></a>
			<a href="keluar.php"><li>Keluar</li></a>
		</ul>
	</header>
	<section>
		<center>
			<font style="font-size: 40px;">Membangun Pemuda Untuk Umat</font>
		</center>
		<img src="itt.jpg">
		<section id="showcase">
		<div class="container">
			<center>
			<h5><font style="font-size: 40px;">PONDOK IT</font></h5>
			
    <p>Pondok IT adalah sebuah tempat pendidikan IT Non formal berbasis pondok yang berada di yogyakarta, Membekali santrinya dengan skill IT & ilmu agama Dan mengarahkan santri belajar sesuai PASSION mereka.</p></center>
<br>
    <center>
			<h5><font style="font-size: 40px;">VISI</font></h5>
			
    <p>"Mejadi lembaga pendidikan terbaik di indonesia yang dapat menciptakan santri yang Bertaqwa,profesional,mandiri dan berbagi."</p></center>
<br>
	<center>
			<h5><font style="font-size: 40px;">MISI</font></h5>
			<p>"Santri memiliki semangat mempelajari agama dan mengamalkannya (betaqwa ). Santri belajar dan bekerja sesuai dengan niat dan bakat ( profesional ). Setelah enam bulan santri dapat mandiri ( mandiri ). Setelah enam bulan santri bisa berbagi untuk pondok ( berbagi ). "</p>
		</center>
		<br>
		<br>
		<div class="w3-container" id="menu">
  <div class="w3-content" style="max-width:700px">
 
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">PERSYARATAN DAN BERKAS PENDAFTARAN</span></h5>
  
    <div class="w3-row w3-center w3-card w3-padding">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Eat');" id="myLink">
        <div class="w3-col s6 tablink">persyaratan</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Drinks');">
        <div class="w3-col s6 tablink">berkas</div>
      </a>
    </div>

    <div id="Eat" class="w3-container menu w3-padding-48 w3-card">
      <h5>1. Laki-Laki/Perempuan</h5>
      <h5>2. Islam</h5>
      <h5>3. Belum Menikah</h5>
      <h5>4. Lulusan SMK/SMA/MA/PONDOK/D3</h5>
      <h5>5. Tidak mampu</h5>
      <h5>6. Mendapat Izin Dan Ridho Dari Orang Tua</h5>
      <h5>7. Tidak Sedang Bekerja Dan Kuliah</h5>
      <h5>8. Usia Minimal 18 Tahun</h5>
      <h5>9. Usia Maksimal 23 Tahun</h5>
      <h5>10. Memiliki Laptop (Milik Sendiri Atau Pinjam Ke Kerabat)</h5>
      <h5>11. Memiliki Minat/Passion Di Bidang IT</h5>
      <h5>12. Memiliki Semangat Menuntut Ilmu Agama</h5>
      <h5>13. Siap Untuk Belajar, Berkarya Dan Berbagi Di Pondok IT Selama 3 Tahun</h5>
      <h5>14. Siap Untuk Berjuang Mengembangkan Pondok IT</h5>
      <h5>15. Mengisi Form Menggunakan Browser Chrome</h5>
      <h5>16. Mengisi Form Menggunakan PC/Desktop</h5>
 
      <h5>17. Surat  Pernyataan Izin Orang Tua</h5>
      <h5>18. Pas Foto Terbaru ukuran 4x6 (maks. 500kb)</h5>
      </div>  
  </div>
</div><br>
<div class="w3-container" id="where" style="padding-bottom:32px;">
  <center><div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">LOKASI PONDOK IT</span></h5>
    <div class="w3-row w3-padding-32"><br>
      <div class="w3-col m6 w3-large w3-margin-bottom">
        <p>Glagah Lor RT.02 No.64 , Desa Tamanan, Kec. Banguntapan, Kab. Bantul, Daerah Istimewa Yogyakarta Pusat Tirtohargo, Kretek, Bantul Regency, Special Region of Yogyakarta 55772, Indonesia Cabang <br></p>
        <i class="fa fa-phone" style="width:30px"></i> Phone:<br> ikhwan: 0895-2800-2800<br> akhwat:0811-8312-312<br>
        <i class="fa fa-envelope" style="width:30px"> </i> <br>Email: info@pondokit.com
      </div></div></div></center>

	</section>

	</section>
	<footer>
		<br>
		Copyright &copy; 2020 Rahmi Refani.
	</footer>

</body>
</html>
<?php } ?>